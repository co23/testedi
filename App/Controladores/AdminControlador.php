<?php  namespace App\Controladores\AdminControlador;

    use App\Modelos\Usuarios;


    if(isset($_POST['usuario']) && isset($_POST['clave'])){

        $usuario=$_POST['usuario'];
        $clave =$_POST['clave'];

     
        $UsuarioClase = new Usuarios();

        $resultado = $UsuarioClase->comprobarClave($usuario,$clave);
        
        if($resultado){

            $_SESSION['username'] = $resultado['usuario'];
            $_SESSION['id'] =  $resultado['id'];
            
            header("Location: inicio");
            //include_once("App/Controladores/admin/inicioAdminControlador.php");
            

        }else{
            echo "los datos no conciden";
            include_once("vista/admin/template/paginas/login.view.php");
        }

    }else{

        include_once("vista/admin/template/paginas/login.view.php");
    }

    
?>