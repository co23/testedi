<?php

    define('ROOT',dirname(__FILE__));
    define('DS',DIRECTORY_SEPARATOR);

    spl_autoload_register('autoload');

    function autoload($class){
        //echo $class."<br>";
        //$class = ROOT.DS.str_replace("\\",DS,$class).'.php';

        $class=$class.".php";

        if(!file_exists($class)){
            throw new Exception("error al cargar la clase". $class);
        }

        require_once($class);

    }


?>